from src.app.sort_base import bubble_sort, insert_sort, heap_sort, timsort
import pytest

Key = lambda x: x[1]
arr = [(6, -56), (4, 18), (9, -32), (7, -33), (2, -15),
       (3, -60), (19, 15), (18, -57), (20, 48), (8, -50),
       (2, 26), (1, -54), (14, -35), (12, 46), (16, -9),
       (1, -1), (15, 7), (10, 23), (15, -52), (6, -18)]


def test_input_data():
    try:
        sorted(arr, key=Key)
    except IndexError:
        assert False
    else:
        assert True


def test_sorted():
    assert all([bubble_sort(arr) == sorted(arr),
               insert_sort(arr) == sorted(arr),
               heap_sort(arr) == sorted(arr),
                timsort(arr) == sorted(arr)])


def test_reversed_sorted():
    assert all([(bubble_sort(arr, reverse=True) == sorted(arr, reverse=True),
                insert_sort(arr, reverse=True) == sorted(arr, reverse=True),
                heap_sort(arr, reverse=True) == sorted(arr, reverse=True),
                timsort(arr, reverse=True) == sorted(arr, reverse=True))])


def test_key_sorted():
    assert all([bubble_sort(arr, key=lambda x: x) == sorted(arr, key=lambda x: x),
               insert_sort(arr, key=lambda x: x) == sorted(arr, key=lambda x: x),
                heap_sort(arr, key=lambda x: x) == sorted(arr, key=lambda x: x),
                timsort(arr, key=lambda x: x) == sorted(arr, key=lambda x: x)])


def test_key_reversed_sorted():
    return all([bubble_sort(arr, key=lambda x: x, reverse=True) == sorted(arr, key=Key, reverse=True),
                insert_sort(arr, key=lambda x: x, reverse=True) == sorted(arr, key=Key, reverse=True),
                heap_sort(arr, key=lambda x: x, reverse=True) == sorted(arr, key=Key, reverse=True),
                timsort(arr, key=lambda x: x, reverse=True) == sorted(arr, key=Key, reverse=True)])

