from src.app.benchmark import benchmark_util
import matplotlib.pyplot as plt
from src.app.benchmark.bench_constants import FIRST, LAST, STEP, LEN_ARRAY, theoretical_complexity, \
    dict_algorithms, name_algorithms


def calls(choice):
    for e in choice:
        fig = plt.figure()
        fig.set_size_inches(12, 9, forward=True)
        calls_lst = benchmark_util.lst_operation(dict_algorithms[e], [FIRST, LAST, STEP], 'swap')
        plt.subplot()
        plt.title(f"Сложность({name_algorithms[e]})")
        benchmark_util.get_graph(LEN_ARRAY, calls_lst,
                                        2, 'Эксперем. сложность', '-k', 'ok')
        benchmark_util.get_graph(LEN_ARRAY, theoretical_complexity[e](LEN_ARRAY),
                                     2, 'Теор. сложность(худшая)', '-k', '+k')
        plt.xlabel('длина массива, шт')
        plt.ylabel('Кол-во операций, шт')

        plt.savefig(f"graphs/{name_algorithms[e]}.png")


