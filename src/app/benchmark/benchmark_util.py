import random
import matplotlib.pyplot as plt
from numpy import poly1d as np_poly1d, polyfit as np_polyfit
from timeit import default_timer
import cProfile
import pstats


def lst_operation(func, len, flag):
    result = []
    for i in range(len[0], len[1]+1, len[2]):
        lst = [(random.randint(-64, 63)) for n in range(i)]
        pr = cProfile.Profile()
        pr.enable()
        func(lst)
        pr.disable()
        with open('output.txt', 'w') as log_file_stream:
            p = pstats.Stats(pr, stream=log_file_stream)
            p.strip_dirs().sort_stats().print_stats(flag)
        f = open('output.txt')
        lines = [line.strip() for line in f]
        lines_1 = [line for line in lines if
                   line not in '' and line.split()[0].isnumeric()]
        for line in lines_1:
            if line.find(flag) != -1:
                result.append(int(line.split('   ')[0]))
        f.close()
    return result


def get_graph(x_lst, y_lst, deg, leg,
              line_1='-r', line_2='--b'):
    trend = np_poly1d(np_polyfit(x_lst, y_lst, deg))
    plt.plot(x_lst, trend(x_lst), line_1)
    plt.plot(x_lst, y_lst, line_2, label=leg)
    plt.legend()
    return trend


def elapsed_time(function, array):
    result = 0
    start_clock = default_timer()
    function(array)
    end_clock = default_timer() - start_clock
    result += end_clock
    return result


