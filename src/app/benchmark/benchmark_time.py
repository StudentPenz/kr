from random import randint
import matplotlib.pyplot as plt
from src.app.benchmark.benchmark_util import elapsed_time
from src.app.benchmark.bench_constants import FIRST, LAST, STEP, LEN_ARRAY_time, \
    dict_algorithms, name_algorithms

def time(choice):
    for e in choice:
        time = []
        for i in range(FIRST, LAST, STEP):
            arr = [(n, randint(-64, 63)) for n in range(i)]
            time.append(elapsed_time(dict_algorithms[e], arr))
        fig, ax = plt.subplots()
        ax.set_title('Benchmark Time')
        ax.set_xlabel('Размер массива')
        ax.set_ylabel('Время выполнения')
        ax.plot(LEN_ARRAY_time, time, linewidth=3, label=f"{name_algorithms[e]}")
        plt.legend()
        plt.savefig(f"graphs/time_{name_algorithms[e]}.png")
