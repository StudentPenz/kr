from math import log
from src.app.sort_base import bubble_sort, insert_sort, heap_sort, timsort

def quadratic_complexity(array):
    return [e**2 for e in array]


def n_logarithm_n_complexity(array):
    return [e*log(e, 2) for e in array]


FIRST = 10000
LAST = 100000
STEP = 10000
LEN_ARRAY = [l for l in range(FIRST, LAST+1, STEP)]
LEN_ARRAY_time = [l for l in range(FIRST, LAST+1, STEP)]

name_algorithms = {
    '1': 'Пузырьковая сортировка',
    '2': 'Сортировка вставками',
    '3': 'Пирамидальная сортировка',
    '4': 'Timsort'}

dict_algorithms = {
    '1': bubble_sort,
    '2': insert_sort,
    '3': heap_sort,
    '4': timsort}

theoretical_complexity = {
    '1': quadratic_complexity,
    '2': quadratic_complexity,
    '3': n_logarithm_n_complexity,
    '4': n_logarithm_n_complexity}








