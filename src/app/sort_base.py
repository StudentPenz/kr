from src.app.support_func import merge, heapify, InsSort, swap, replace


def bubble_sort(lst, key=lambda x: x, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        n = len(lst)
        swapped = True
        while swapped:
            swapped = False
            for i in range(n - 1):
                if (key(lst[i]) > key(lst[i + 1])):
                    swap(lst, i, i+1)
                    swapped = True
            n -= 1

        if reverse:
            return lst[::-1]
        else:
            return lst


def heap_sort(lst, key=lambda x: x, reverse = False):
    n = len(lst)

    for i in range(n, -1, -1):
        heapify(lst, n, i)

    for i in range(n - 1, 0, -1):
        swap(lst, i, 0)
        heapify(lst, i, 0, key=key)
    if reverse:
        return lst[::-1]
    else:
        return lst


def insert_sort(lst, key=lambda x: x, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        for i in range(1, len(lst)):
            first = lst[i]
            j = i - 1
            while j >= 0 and (key(lst[j]) > first):
                replace(lst, j + 1, j)
                j -= 1
            replace(lst, j + 1, first, choice=1)
        if reverse:
            return lst[::-1]
        else:
            return lst


def timsort(lst, key=lambda x: x, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        minrun_const = 32
        n = len(lst)
        r = 0
        while n >= minrun_const:
            r |= n & 1
            n >>= 1
        minrun = n + r

        for start in range(0, n, minrun):
            end = min(start + minrun - 1, n - 1)
            lst = InsSort(lst, start, end, key=key)

        curr_size = minrun
        while curr_size < n:
            for start in range(0, n, curr_size * 2):
                mid = min(n - 1, start + curr_size - 1)
                end = min(n - 1, mid + curr_size)
                lst = merge(lst, start, mid, end)
            curr_size *= 2
        if reverse:
            return lst[::-1]
        else:
            return lst