def swap(lst, first, second):
    lst[first], lst[second] = lst[second], lst[first]


def replace(lst, first, second, choice=0):
    if choice == 0:
        lst[first] = lst[second]
    elif choice == 1:
        lst[first] = second


def merge(lst, start, mid, end):
    if mid == end:
        return lst
    first = lst[start:mid + 1]
    last = lst[mid + 1:end + 1]
    len1 = mid - start + 1
    len2 = end - mid
    ind1 = 0
    ind2 = 0
    ind = start

    while ind1 < len1 and ind2 < len2:
        if first[ind1] < last[ind2]:
            lst[ind] = first[ind1]
            ind1 += 1
        else:
            lst[ind] = last[ind2]
            ind2 += 1
        ind += 1

    while ind1 < len1:
        lst[ind] = first[ind1]
        ind1 += 1
        ind += 1

    while ind2 < len2:
        lst[ind] = last[ind2]
        ind2 += 1
        ind += 1

    return lst


def InsSort(lst, start, end, key=lambda x: x):
    if len(lst) <= 1:
        return lst
    else:
        for i in range(start+1, end+1):
            j = i - 1
            while j >= start and (key(lst[j]) > key(lst[i])):
                lst[j + 1] = lst[j]
                j -= 1
            lst[j + 1] = lst[i]
        return lst


def heapify(lst, heap_size, index,key=lambda x: x):
    largest = index
    left_child = (2 * index) + 1
    right_child = (2 * index) + 2

    if left_child < heap_size and key(lst[left_child]) > key(lst[largest]):
        largest = left_child

    if right_child < heap_size and key(lst[right_child]) > key(lst[largest]):
        largest = right_child

    if largest != index:
        lst[index], lst[largest] = lst[largest], lst[index]
        heapify(lst, heap_size, largest)