from src.app.benchmark import benchmark_calls, benchmark_time


def main():

    print("Выберите нужные алгоритмы:\n",
          "1 - Пузырьковая сортировка\n",
          "2 - Сортировка вставками\n",
          "3 - Пирамидальная сортировка\n",
          "4 - Timsort\n")

    choice = (input("Введите номера алгоритмов через пробел >> \n")).split()
    try:
        benchmark_calls.calls(choice)
        benchmark_time.time(choice)
    except KeyError:
        raise ValueError('Некорректный ввод')
    print("Результаты работы программы можно наблюдать на сохраненных графиках выбранных сортировок.")

if __name__ == "__main__":
    main()
