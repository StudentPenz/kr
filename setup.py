from setuptools import setup, find_packages


setup(name='AIPCourseWork',
      version='1.0',
      description='Python example library',
      author='Andrew Roldugin',
      author_email='rolandrey23@gmail.com',
      url='',
      packages=find_packages()
)
